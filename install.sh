#!/usr/bin/env bash

sudo apt update -y

# python
sudo apt install -y python3 python3-pip python-is-python3

echo "####################################################################################"
echo "Random stuff"
echo "####################################################################################"
#sudo apt install -y git mesa-utils curl rofi xclip nitrogen rsync htop alsa-utils man nodejs npm ncdu xbacklight pavucontrol udiskie xsettingsd dunst shellcheck
sudo apt install -y git curl rofi rsync htop alsa-utils man nodejs npm ncdu xsettingsd shellcheck bluez gparted xsettingsd rustc cargo pavucontrol xclip

# Flatpak
# Flathub is the best place to get Flatpak apps
sudo apt install flatpak
flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo

# Nvidia drivers installation
# https://help.ubuntu.com/community/NvidiaDriversInstallation
#sudo ubuntu-drivers install

bash scripts/setup_backlightCtrl.sh

sudo systemctl disable apport.service

git submodule update --init --recursive

# Access serial ports
sudo usermod -a -G dialout $USER

# Copy configuration files
echo "####################################################################################"
echo "Copy configuration files"
echo "####################################################################################"
bash scripts/copyConfig.sh

echo "####################################################################################"
echo "Terminal suff"
echo "####################################################################################"
sudo apt install -y neovim python3-neovim qalc fzf ripgrep fd-find tree sshfs
cargo install starship --locked
cargo install tokei
# flatpak install flathub io.neovim.nvim
# flatpak run io.neovim.nvim
# Note that Flatpak'ed Neovim will look for init.vim in `~/.var/app/io.neovim.nvim/config/nvim` instead of `~/.config/nvim`.

# sudo snap install git-delta-snap

# hstr for Ubuntu 21.04 and older
# sudo add-apt-repository ppa:ultradvorka/ppa
# sudo apt update
# sudo apt install -y hstr
sudo apt install hstr

# Set git editor to be nvim
git config --global core.editor nvim

echo "####################################################################################"
echo "Terminal emulator"
echo "####################################################################################"
bash scripts/setup_alacritty.sh

echo "####################################################################################"
echo "GUI file manager"
echo "####################################################################################"
# https://wiki.archlinux.org/title/thunar
sudo apt install -y thunar gvfs thunar-archive-plugin thunar-media-tags-plugin thunar-volman tumbler ffmpegthumbnailer

echo "####################################################################################"
echo "GUI code editor"
echo "####################################################################################"
# Doom Emacs
bash scripts/setup_emacs.sh

# VSCodium
snap install codium --classic

echo "####################################################################################"
echo "Media programs"
echo "####################################################################################"
sudo apt install -y vlc flameshot

echo "####################################################################################"
echo "Other GUI apps"
echo "####################################################################################"
sudo apt install -y redshift redshift-gtk qbittorrent libreoffice gimp okular nextcloud-desktop arandr obs-studio
#sudo snap install qalculate
# flatpak install flathub io.github.Qalculate.qalculate-qt  # Qt UI
flatpak install flathub io.github.Qalculate  # GTK UI
# flatpak install flathub org.kde.okular
# flatpak install flathub com.jgraph.drawio.desktop
sudo pip install autorandr

echo "####################################################################################"
echo "Browser"
echo "####################################################################################"
bash scripts/setup_libreWolf.sh

echo "####################################################################################"
echo "Window manager"
echo "####################################################################################"
# libiw-dev - https://stackoverflow.com/a/36233632
#sudo apt install -y libiw-dev  # Needed for Wlan widget (iwlib)
#pip install iwlib xcffib psutil
#pip install pyxdg dbus-next xlib
#pip install qtile
#sudo cp qtile.desktop /usr/share/xsessions/

# https://stackoverflow.com/a/70694565
#sudo apt install -y libffi-dev
#pip install --force-reinstall --no-binary :all: cffi

sudo apt install wmctrl feh polybar dunst inotify-tools blueman

git clone https://github.com/leftwm/leftwm.git ~/leftwm
(cd SOME_PATH && exec_some_command)
( \
    cd ~/leftwm \
    && cargo build --profile optimized \
    && sudo install -s -Dm755 ./target/optimized/leftwm ./target/optimized/leftwm-worker ./target/optimized/lefthk-worker ./target/optimized/leftwm-state ./target/optimized/leftwm-check ./target/optimized/leftwm-command -t /usr/bin \
    && sudo cp leftwm.desktop /usr/share/xsessions/ \
)

# Display locker
sudo apt install i3lock

echo "####################################################################################"
echo "Password manager"
echo "####################################################################################"
sudo add-apt-repository ppa:phoerious/keepassxc
sudo apt update -y
sudo apt install -y keepassxc
# https://github.com/keepassxreboot/keepassxc/issues/6907#issuecomment-962638332
ln -s ~/.mozilla/native-messaging-hosts ~/.librewolf/native-messaging-hosts
# flatpak install org.keepassxc.KeePassXC

echo "####################################################################################"
echo "Themes"
echo "####################################################################################"
sudo apt install breeze-icon-theme
sudo snap install adw-gtk3-theme
flatpak install flathub org.kde.KStyle.Adwaita
for i in $(snap connections | grep gtk-common-themes:gtk-3-themes | awk '{print $2}'); do
    echo "  - $i"
    sudo snap connect "$i" adw-gtk3-theme:gtk-3-themes;
done

echo "####################################################################################"
echo "Setting default applications"
echo "####################################################################################"
# https://www.reddit.com/r/linuxquestions/comments/5z3n81/default_applications_in_arch_linux/dev24vo?utm_source=share&utm_medium=web2x&context=3
# Location: /usr/share/applications/
# Get default for specific file: xdg-mime query filetype myImage.jpg
# Set default: xdg-mime default sxiv.desktop image/jpg
# Get default: xdg-mime query default application/pdf
#xdg-mime default sxiv.desktop image/jpeg
#xdg-mime default sxiv.desktop image/jpg
#xdg-mime default sxiv.desktop image/png
#xdg-mime default sxiv.desktop image/svg+xml
#xdg-mime default sxiv.desktop image/gif
#xdg-mime default sxiv.desktop image/bmp
#xdg-mime default pcmanfm.desktop inode/directory
xdg-mime default okularApplication_pdf.desktop application/pdf
xdg-mime default emacsclient.desktop text/plain
xdg-mime default librewolf.desktop text/html
xdg-mime default librewolf.desktop x-scheme-handler/http
xdg-mime default librewolf.desktop x-scheme-handler/https
xdg-mime default librewolf.desktop application/json

echo "####################################################################################"
echo "Copying xorg keyboard configuration files"
echo "####################################################################################"
# https://wiki.archlinux.org/index.php/Xorg/Keyboard_configuration
#sudo cp 00-keyboard.conf /etc/X11/xorg.conf.d/
#sudo cp 00-keyboard.conf /usr/share/X11/xorg.conf.d/
sudo cp keymap/xkb/symbols/si_colemak /usr/share/X11/xkb/symbols/

echo "####################################################################################"
echo "Touchpad: change scrolling direction, enable tapping..."
echo "####################################################################################"
#sudo cp 40-libinput.conf /etc/X11/xorg.conf.d/
sudo cp 00-libinput.conf /usr/share/X11/xorg.conf.d/

echo "####################################################################################"
echo "Laptop backlight control"
echo "####################################################################################"
#sudo cp 00-backlight.conf /usr/share/X11/xorg.conf.d/

echo "####################################################################################"
echo "Set max usage in journald.conf"
echo "####################################################################################"
sudo sed -i '/#SystemMaxUse=/c\SystemMaxUse=100M' /etc/systemd/journald.conf

# Copy wallpapers collection
echo "####################################################################################"
echo "Copy wallpapers collection"
echo "####################################################################################"
#cp -r ./wallpapers-collection ~/wallpapers-collection

# Setup snapper (btrfs)
echo "####################################################################################"
echo "Setup snapper"
echo "####################################################################################"
sudo apt install -y btrfs-progs snapper-gui
#bash scripts/snapperSetup.sh

# This must be called after `ssh-agent.service` is copied to `~/.config/systemd/user/`
# This must be called after `playerctld.service` is copied to `~/.config/systemd/user/`
echo "####################################################################################"
echo "Enable custom systemd services"
echo "####################################################################################"
systemctl enable --user ssh-agent
# systemctl enable --user playerctld
