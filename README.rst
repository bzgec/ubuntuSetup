===================
Arch Linux Setup
===================

:info: My Ubuntu Setup

:Authors:
    Bzgec

.. role:: bash(code)
   :language: bash

.. contents:: Table of Contents
   :depth: 2


Usefull info
============

Get key events/codes
--------------------
* :bash:`xev` (xorg-xev package)

Mount disk at boot
------------------
We are doing the right way - with :bash:`fstab`

1. Get UUID of the disk: :bash:`ls -al /dev/disk/by-uuid/`
2. Get file system format of the partition: :bash:`file -sL /dev/sd*`
3. Configure :bash:`fstab` file: :bash:`sudo vim /etc/fstab`
   ::

     /etc/fstab
     ----------------------------------------------------------------------------------------------------
     # Static information about the filesystems.
     # See fstab(5) for details.

     # <file system> <dir> <type> <options> <dump> <pass>
     # Windows - /dev/nvme0n1p3
     UUID=C25684FB5684F189  /home/USER/win10  ntfs  rw,nosuid,nodev,user_id=0,group_id=0,allow_other,blksize=4096  0 0

     # Home - /dev/nvme0n1p7
     UUID=20cce99b-5a1f-4e98-9a1e-351f31df1c4c  /home  btrfs  rw,noatime,compress=lzo,ssd,space_cache=v2,subvolid=257  0 0

     # Swap - /dev/nvme0n1p6
     UUID=eb530593-0307-4678-a1f2-9c9065574950  none  swap  defaults  0 0

     # Internal HDD - /dev/sda1
     UUID=7D524647407BEC2A  /home/USER/SlimBoi  ntfs  defaults  0 0

New btrfs disk
--------------

1. :bash:`sudo mkfs.btrfs -f -L projects /dev/nvme1n1p3`
2. :bash:`mount /dev/nvme1n1p3 /mnt`
3. :bash:`btrfs su cr /mnt/@`
4. :bash:`btrfs su cr /mnt/@snapshots`
5. :bash:`umount /mnt`
6. :bash:`mount -o subvol=@ /dev/nvme1n1p3 /mnt`
7. :bash:`mkdir -p /mnt/.snapshots`
8. :bash:`mount -o subvol=@snapshots /dev/nvme1n1p3 /mnt/.snapshots`

9. :bash:`ls -al /dev/disk/by-uuid/`

   .. code::

      85914935-3e4b-4339-a8b4-c7a316a0ce28 -> ../../nvme1n1p3

10. :bash:`sudo btrfs subvolume list /mnt`

    .. code::

       ID 256 gen 11 top level 5 path @
       ID 257 gen 8 top level 5 path @snapshots

11. :bash:`nvim /etc/fstab`

    .. code::

       # projects - /dev/nvme1n1p3
       UUID=85914935-3e4b-4339-a8b4-c7a316a0ce28  /home/bzgec/projects  btrfs  rw,noatime,compress=lzo,ssd,space_cache=v2,subvolid=256  0 0
       UUID=85914935-3e4b-4339-a8b4-c7a316a0ce28  /home/bzgec/projects/.snapshots  btrfs  rw,noatime,compress=lzo,ssd,space_cache=v2,subvolid=257  0 0

12. Reboot and check if new btrfs is OK:

    - sudo btrfs filesystem usage /home/bzgec/projects
    - sudo btrfs filesystem usage /
    - sudo btrfs subvolume list /home/bzgec/projects
    - sudo btrfs subvolume list /

      .. code::

         ID 256 gen 11 top level 5 path @
         ID 257 gen 8 top level 5 path @snapshots

13. Snapper setup

    1. :bash:`sudo umount /home/bzgec/projects/.snapshots`
    2. :bash:`sudo rm -rf /home/bzgec/projects/.snapshots`
    3. :bash:`sudo snapper -c home create-config /projects`
    4. :bash:`sudo btrfs subvolume delete /home/bzgec/projects/.snapshots`
    5. :bash:`sudo mkdir /home/bzgec/projects/.snapshots/`
    6. :bash:`sudo mount -a`
    7. :bash:`sudo chmod 750 /home/bzgec/projects/.snapshots`
    8. :bash:`sudo chmod a+rx /home/bzgec/projects/.snapshots`
    9. :bash:`sudo chown bzgec /home/bzgec/projects/.snapshots`
    10. Edit snapper config: :bash:`nvim /etc/snapper/configs/projects`

NTFS partition permissions
--------------------------

In order to set permissions to NTFS partition mount as (note the ``permissions`` option):

::

  UUID=235639C5768BC70B   /home/bzgec/storage     ntfs        rw,auto,user,permissions,blksize=4096   0 0

Resources
~~~~~~~~~
- `random post <https://confluence.jaytaala.com/display/TKB/Mount+drive+in+linux+and+set+auto-mount+at+boot>`__
- `get partition (fs) format - unix.stackexchange.com <https://unix.stackexchange.com/a/60783>`__

Disable microphone
------------------
* `ALSA - Advanced Linux Sound Architecture <https://wiki.archlinux.org/index.php/Advanced_Linux_Sound_Architecture>`__
* Toggle microphone: :bash:`amixer set Capture toggle`

Fix missing GRUB
----------------
1. Boot into live arch USB
2. mount your linux partition:

   - btrfs: :bash:`mount -o noatime,compress=lzo,space_cache=v2,subvol=@ /dev/nvme0n1p7 /mnt`
   - other (not tested): :bash:`mount -t auto /dev/nvme0n1p7 /mnt`

3. mount EFI partition: :bash:`mount /dev/nvme0n1p5 /mnt/boot`
   You can check which one is EFI with :bash:`fdisk -l`
4. change root into system: :bash:`arch-chroot /mnt`
5. recreate kernel image (don't know if necessary): :bash:`mkinitcpio -p linux`
6. install grub:

   - EFI: :bash:`grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB`

7. Generate grub configuration: :bash:`grub-mkconfig -o /boot/grub/grub.cfg (you should see OS images)`

   .. note::
      If you have problem with detecting OS:

      1. edit ``/etc/default/grub``
      2. add or uncomment ``GRUB_DISABLE_OS_PROBER=false``
      3. save that file then run ``grub-mkconfig -o /boot/grub/grub.cfg``

      `reference <https://forum.endeavouros.com/t/warning-os-prober-will-not-be-executed-to-detect-other-bootable-partitions-systems-on-them-will-not-be-added-to-the-grub-boot-configuration-check-grub-disable-os-prober-documentation-entry/13998/2>`__

8. :bash:`exit`
9. :bash:`umount -R /mnt`
10. :bash:`reboot`

USB Stick not detected
----------------------
* If your USB is not detected you probably updated your system,
  now you need to restart your PC and it should work
  ::

    general if your kernel updates, all old modules are removed which in practice means no new hardware is being detected

* `Reference <https://bbs.archlinux.org/viewtopic.php?id=234335>`__

Before formatting
=================

1. Before formatting run :bash:`make backup_config_to_repo` to backup existing configuration
   to the repository.

Development
===========

Dependencies
------------
* ``shellcheck``

Before commiting
----------------
Run :bash:`make check` to check for best coding standards.

