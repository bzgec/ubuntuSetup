=========
Changelog
=========

.. contents:: Table of Contents
   :depth: 1

v1.0.0 - 2023/05/14
===================

- First release

vX.X.X - YYYY/MM/DD
===================

Added
-----

Changed
-------

Fixed
-----

Removed
-------
