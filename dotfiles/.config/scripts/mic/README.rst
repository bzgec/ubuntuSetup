===
mic
===

:Info: Simple Rust script to set and get state of input devices on Linux machines
:Authors:
    Bzgec

.. contents:: Table of Contents
   :depth: 2

.. role:: bash(code)
   :language: bash

Requirements
============

- rust
- make

Install
=======

1. Clone this repository
2. Build executable: :bash:`make`
3. Now you have executable in this folder, named `mic`
