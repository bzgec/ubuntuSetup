#!/usr/bin/env python

import argparse
import subprocess
import re

def get_connected_monitors():
    """Get a dictionary of connected monitors and their current brightness levels."""
    try:
        output = subprocess.check_output(['xrandr', '--verbose']).decode('utf-8')
        lines = output.split('\n')
        monitors = {}
        current_monitor = None
        for line in lines:
            if ' connected' in line:
                current_monitor = line.split()[0]
            elif current_monitor and 'Brightness:' in line:
                brightness = re.findall(r"([0-9.]+)", line)[0]
                monitors[current_monitor] = float(brightness)
        return monitors
    except subprocess.CalledProcessError as e:
        print("Error:", e)
        return {}

def set_brightness(monitor, brightness):
    """Set brightness for a specific monitor."""
    try:
        subprocess.run(['xrandr', '--output', monitor, '--brightness', str(brightness)])
        print(f"Brightness for {monitor} set to {brightness}")
    except subprocess.CalledProcessError as e:
        print("Error:", e)

def main():
    parser = argparse.ArgumentParser(description="Control monitor brightness using xrandr")
    parser.add_argument('brightness', type=int, help="Relative brightness change (-100 to 100)")
    args = parser.parse_args()

    monitors = get_connected_monitors()
    if not monitors:
        print("No monitors detected.")
        return

    if args.brightness < -100 or args.brightness > 100:
        print("Brightness change percentage must be between -100 and 100.")
        return

    for monitor, current_brightness in monitors.items():
        new_brightness = current_brightness + args.brightness / 100.0
        new_brightness = max(0.0, min(1.0, new_brightness))  # Ensure brightness is between 0 and 1
        set_brightness(monitor, new_brightness)

if __name__ == "__main__":
    main()
