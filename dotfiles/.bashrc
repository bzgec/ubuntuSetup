# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
alias ll='ls -halF'
alias la='ls -hA'
alias l='ls -hCF'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi
source /home/askerc/alacritty/extra/completions/alacritty.bash


alias ls='ls --color=auto'
PS1='[\u@\h \W\$ '

export ALTERNATE_EDITOR=""   # setting for emacsclient
export EDITOR="nvim"         # $EDITOR use Emacs in terminal
export VISUAL="nvim"         # $VISUAL use Emacs in GUI mode

# Check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

################################################################################
# Change path
################################################################################
# Neovim
export PATH="$PATH":/opt/nvim/

# Python pip scripts
export PATH="$PATH":~/.local/bin

# Tmux sessions
export PATH="$PATH":~/.config/tmux

# Doom Emacs
export PATH="$PATH":~/.config/emacs/bin

# Cargo
export PATH="$PATH":~/.cargo/bin

# Go
export PATH="$PATH":~/go/bin

# Nodejs
export PATH="$PATH":~/.nvm/versions/node/v20.11.1/bin

# Cppcheck
export PATH="$PATH":~/per/cppcheck/cppcheck/build/bin

# STM32CubeMX
export PATH="$PATH":~/STM32CubeMX
################################################################################

################################################################################
# Bash history management
################################################################################
# https://gist.github.com/marioBonales/1637696
#
# Don't store commands started with space and don't save new command if previous was the same
# ignoreboth == ignorespace and ignoredups
HISTCONTROL=ignoreboth

# Append to the history file, don't overwrite it
shopt -s histappend

# HISTSIZE: The number of commands to remember in the command history (list)
HISTSIZE=50000
# HISTFILESIZE: The  maximum  number of lines contained in the history file
HISTFILESIZE=50000

# PROMPT_COMMAND: contents of this variable is run as regular command before bash show prompt.
#                 So every time after you execute a command contents of this variable is also
#                 executed.
# 'history -a': append history lines from this session to the history file
# 'history -n': read all history lines not already read from the history file
#               and append them to the history list
# 'history -r': read the history file and append the contents to the history list
# export PROMPT_COMMAND="history -a; history -n"
export PROMPT_COMMAND="history -a"

alias bashsynchistorylist="history -r"
################################################################################

################################################################################
# fzf
################################################################################
# Add key-bindings: 'Ctrl+R', 'Ctrl+T', 'Alt+C'
# https://github.com/junegunn/fzf?tab=readme-ov-file#key-bindings-for-command-line
source /usr/share/doc/fzf/examples/key-bindings.bash

# https://github.com/junegunn/fzf?tab=readme-ov-file#fuzzy-completion-for-bash-and-zsh
# source /usr/share/doc/fzf/examples/completion.bash
################################################################################

################################################################################
# Starship
################################################################################
##################################################
# Terminal title
##################################################
# Set termTitle to empty string - if set it is used as terminal title
termTitle=''

# Set tab title funtion
# https://makandracards.com/makandra/21369-bash-setting-the-title-of-your-terminal-tab
# https://unix.stackexchange.com/questions/216953/show-only-current-and-parent-directory-in-bash-prompt
function tabTitleSet {
  # Check if termTitle var is set
  if [[ ! "$termTitle" ]]; then
    # termTitle var not set, check if we are trying to set it
    if [ -z "$1" ]; then
      # Set tab title as current directory
      # title=${PWD##*/}  # Current directory
      # title=${PWD}  # Current directory - full path
      title=$(basename $(dirname "$PWD"))/$(basename "$PWD")  # Current dir and parent dirr
    else
      # set termTitle var
      termTitle=$1  # First param
      title=$1  # first param
    fi
  else
    title=$termTitle
  fi

  echo -ne "\033]0;$title\007"
}

function tabTitleReset {
  termTitle=''
}

# Set cb function when command is executed in terminal
# https://starship.rs/advanced-config/#change-window-title
starship_precmd_user_func="tabTitleSet"

##################################################
# Enable starship prompt
##################################################
eval "$(starship init bash)"
################################################################################

################################################################################
# HSTR configuration - add this to ~/.bashrc
################################################################################
# alias hh=hstr                    # hh to be alias for hstr
# export HSTR_CONFIG=hicolor       # get more colors
# shopt -s histappend              # append new history items to .bash_history
# export HISTCONTROL=ignoreboth    # leading space hides commands from history
# export HISTFILESIZE=10000        # increase history file size (default is 500)
# export HISTSIZE=${HISTFILESIZE}  # increase history size (default is 500)
# # ensure synchronization between bash memory and history file
# export PROMPT_COMMAND="history -a; history -n; ${PROMPT_COMMAND}"
# function hstrnotiocsti {
#     { READLINE_LINE="$( { </dev/tty hstr ${READLINE_LINE}; } 2>&1 1>&3 3>&- )"; } 3>&1;
#     READLINE_POINT=${#READLINE_LINE}
# }
# # if this is interactive shell, then bind hstr to Ctrl-r (for Vi mode check doc)
# if [[ $- =~ .*i.* ]]; then bind -x '"\C-r": "hstrnotiocsti"'; fi
# export HSTR_TIOCSTI=n
################################################################################


gitlog() {
  git log --pretty=format:'%C(yellow)%h %Cred%ad %C(cyan)%an%C(auto)%d %C(reset)%s' --date=format:'%Y/%m/%d %H:%M:%S' --all --graph
}
alias gitlog-date='git log --pretty=fuller'
alias gits='git status -s'
alias git-commit-date-now='git commit --amend --date=now --no-edit'

killproc() {
    ps aux | fzf | awk '{print $2}' | xargs kill
}

copyfolder() {
    command pwd | xclip -r -selection clipboard
}

copyfilename() {
    command ls | fzf | xclip -r -selection clipboard
}

copygithash() {
    git log --pretty=format:'%C(yellow)%h %Cred%ad %C(cyan)%an%C(auto)%d %C(reset)%s' --date=format:'%Y/%m/%d %H:%M:%S' --all | fzf | awk '{print $1}' | xclip -r -selection clipboard
}

serial-check() {
    if ls /dev/ttyUSB* 1> /dev/null 2>&1; then
        ls /dev/ttyUSB*
    else
        echo "No /dev/ttyUSB* devices"
    fi

    if ls /dev/ttyACM* 1> /dev/null 2>&1; then
        ls /dev/ttyACM*
    else
        echo "No /dev/ttyACM* devices"
    fi
}

serial-copy() {
    if ls /dev/ttyUSB* 1> /dev/null 2>&1 || ls /dev/ttyACM* 1> /dev/null 2>&1; then
        ls /dev/tty* | grep -E '/dev/ttyUSB|/dev/ttyACM' | fzf | xclip -r -selection clipboard
    fi
}

alias light="~/.config/scripts/themeSwitcher.py --light"
alias dark="~/.config/scripts/themeSwitcher.py --dark"

alias keymap="~/.config/scripts/keymap.sh"

alias tmuxs="tmux-sessionizer"

alias vpn-conn="wg-quick up wg0"
alias vpn-dis="wg-quick down wg0"

alias get-ip="curl https://icanhazip.com/"

alias reload-wm="leftwm-command SoftReload"
# alias lock-suspend="slock & systemctl suspend"
# alias lock-suspend="i3lock -fe --color=000000 & systemctl suspend"
alias lock-suspend="~/.config/scripts/screen-locker/lock-screen.sh & systemctl suspend"

# alias lock-hibernate="sudo su & slock & systemctl hibernate"
# alias lock-hibernate="sudo su & i3lock -fe --color=000000 & systemctl hibernate"

alias autodisplay="autorandr -c"
alias mobile="autorandr -l mobile"
alias docked="autorandr -l docked-work-2displays || autorandr -l docked-work-2displays-2"
alias docked2="autorandr -l docked-work-2displays-2"
alias docked3="autorandr -l docked-work-3displays"
