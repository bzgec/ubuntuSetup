#!/usr/bin/env bash

mkdir -p ~/repos

sudo apt install -y hunspell hunspell-sl ripgrep fd-find
sudo snap install emacs --classic
git clone --depth 1 https://github.com/doomemacs/doomemacs ~/.config/emacs
~/.config/emacs/bin/doom install
rm -rf ~/.doom.d
