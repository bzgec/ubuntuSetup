#!/bin/bash
# Copy important configuration from current PC to dotfiles/

configToBackup=(
    '.bashrc'
    '.profile'
    '.xprofile'
    '.gitconfig'
    '.gtkrc-2.0'
    '.config/alacritty/alacritty.toml'
    '.config/autorandr/'
    #'.config/doom/'
    #'.config/dunst/'
    '.config/gtk-3.0/settings.ini'
    '.config/keepassxc/'
    '.config/qt5ct/qt5ct.conf'
    #'.config/qtile/'
    #'.config/redshift/'
    '.config/rofi/'
    #'.config/VSCodium/User/keybindings.json'
    #'.config/VSCodium/User/settings.json'
    '.config/starship.toml'
    '.config/systemd/user/'
    '.config/xsettingsd/xsettingsd.conf'
    '.config/leftwm'
    '.config/scripts'
    '.config/starship.toml'
    '.config/picom'
    '.config/tmux/'
    '.tmux.conf'
)

scriptPath="$(cd -- "$(dirname "$0")" || exit 1 >/dev/null 2>&1 ; pwd -P )"
parentFolder="$(dirname "$scriptPath")"
dotfilesFolder="$parentFolder/dotfiles"

(
    cd "$HOME" || exit
    for toCopy in "${configToBackup[@]}"
    do
       echo '~'"/$toCopy"
       rsync -a --delete --relative "$toCopy" "$dotfilesFolder" --exclude=.git --exclude=__pycache__ --exclude=.mypy_cache
       #rsync --exclude=.git --exclude=__pycache__ --exclude=.mypy_cache -r "$HOME/$toCopy" "$dotfilesFolder/$toCopy"
    done
)
