#!/usr/bin/env bash

# https://gitlab.com/wavexx/acpilight
# https://gitlab.com/ddcci-driver-linux/ddcci-driver-linux

#mkdir -p ~/repos
#
#git clone https://gitlab.com/wavexx/acpilight.git ~/repos/acpilight
#
#(
#    cd ~/repos/acpilight || exit
#    sudo make install
#)
#
#sudo usermod -a -G video $LOGNAME
#
#sudo cp 90-backlight.rules /etc/udev/rules.d/


# https://github.com/Hummer12007/brightnessctl
sudo apt install -y brightnessctl
sudo usermod -a -G video "$LOGNAME"
