#!/usr/bin/env bash

# https://github.com/alacritty/alacritty/blob/master/INSTALL.md

# Dependencies
sudo apt install -y rustc cargo
sudo apt install -y cmake pkg-config libfreetype6-dev libfontconfig1-dev libxcb-xfixes0-dev libxkbcommon-dev python3
sudo apt install -y terminfo

#cargo install alacritty
(\
    cd ~ \
    && git clone https://github.com/alacritty/alacritty.git \
    && cd alacritty \
    && echo "If build fails, move to latest tag" \
    && cargo build --release \
    && echo "If build fails, move to latest tag" \
    && echo "Terminfo" \
    && sudo tic -xe alacritty,alacritty-direct extra/alacritty.info \
    && echo "Desktop Entry" \
    && sudo cp target/release/alacritty /usr/bin \
    && sudo cp extra/logo/alacritty-term.svg /usr/share/pixmaps/Alacritty.svg \
    && sudo desktop-file-install extra/linux/Alacritty.desktop \
    && sudo update-desktop-database \
    && echo "Shell completions" \
    && echo "source $(pwd)/extra/completions/alacritty.bash" >> ~/.bashrc \
)
